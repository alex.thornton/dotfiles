#!/usr/bin/env bash

brew update
brew cask reinstall dbeaver-community

DBEAVER_INI="/Applications/DBeaver.app/Contents/Eclipse/dbeaver.ini"
if grep -q -- "-vmargs" $DBEAVER_INI; then 
	if ! grep -q -- "-Duser.timezone=UTC" $DBEAVER_INI; then 
		echo "-Duser.timezone=UTC" >> $DBEAVER_INI
	fi
fi